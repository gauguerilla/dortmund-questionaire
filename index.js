

Vue.component('v-select', VueSelect.VueSelect); 

Vue.component('qinput', {
    // The todo-item component now accepts a
    // "prop", which is like a custom attribute.
    props: ['question'],
    template: `
    <div v-bind:class=question.classes>
    <span>{{question.question}}</span><br>
    <v-select class="preselect" v-if="question.preselect" @input="question.preselectOutputMethod($event,question)" v-model="question.preselected" :options="question.preselect"></v-select>
    <input v-if="(question.type=='text' || !question.type) && !question.hidden" :placeholder=question.placeholder v-model="question.result" v-on:input="output1(question)">
    <textarea v-if="(question.type=='multiline') && !question.hidden" :placeholder=question.placeholder v-model="question.result" v-on:input="output1(question)" class="vueMultiline"></textarea>
    <v-select v-if="question.type=='select' && !question.hidden" v-model="question.result" v-on:input="output1(question)" :options="question.selectValues"></v-select>
    <br><br>
    </div>`,
    methods:{
        output1: function(inp){ // here we can modify the inputs from ALL inputs.
            if (inp.outputMethod)
            {
                inp.output = inp.outputMethod(inp.result) // inside outputMethod of questionaire we can modify inp for only this question
            }
            else
            {
                inp.output = inp.result
            }
            if (inp.result.length < 1){
                inp.output = "";
            }

            if (inp.sideboxOutputMethod)
            {
                inp.sideboxoutput = inp.sideboxOutputMethod(inp.result) // inside outputMethod of questionaire we can modify inp for only this question
            }
            else
            {
                inp.sideboxoutput = inp.result
            }
            if (inp.result.length < 1){
                inp.sideboxoutput = "";
            }
            } 
    }
  })

var vm = new Vue({
    el: '#app',
    data:{
        questionaire: [
            {
                question:"What is the title of your project?",
                result: "",
                output: "",
                outputMethod: function(inp){return "# "+inp},
                sideboxoutput:"",
                sideboxOutputMethod: function(inp){return "#### "+inp}
            },
            {
                question:"Do you have a small logo or picture that represents your project?",
                preselect: ["YES","NO"],
                preselectOutputMethod: function(inp,quest){
                        quest.hidden= !(inp == "YES");
                        quest.placeholder="Please provide hyperlink to your logo/pic!"
                },
                hidden: true,
                result: "",
                output: "",
                outputMethod: function(inp){return ""},
                sideboxoutput:"",
                sideboxOutputMethod: function(inp){
                    url = inp.trim();
                    let out = "";
                    if (url)
                    {
                        out = `&lt;img src="${url}" width="90%"&gt;`;
                    }
                    return out;
                }
            },
            {
                question:"what type of project is it?",
                type:"select",
                selectValues:["fellow project","playON project","independent"],
                result: "",
                output: "",
                outputMethod: function(inp){
                    if (inp == "fellow project"){checkFellowProject()}
                    return ""
                },
                sideboxoutput:"",
                sideboxOutputMethod: function(inp){return `**type of project**: ${inp}`}
            },
            {
                question:"Please describe your project in a few sentences (3-5 is optimal!) Just a little teaser, so people might get interested...",
                type: "multiline",
                result: "",
                output: "",
                outputMethod: function(inp){return "<pre>"+escapeHtml( inp.replace(/(?:\r\n|\r|\n)/g, '\n<br>') )+"</pre>"}
            },
            {
                question:"Do you want to describe some Goals of your project? e.g. the political implications or research questions that led to the project?",
                classes: "fellow",
                preselect: ["YES","NO"],
                preselectOutputMethod: function(inp,quest){
                        quest.hidden= !(inp == "YES");
                        quest.placeholder="Please describe your Goal(s) and/or research questions in free text!"
                },
                hidden: true,
                type: "multiline",
                result: "",
                output: "",
                outputMethod: function(inp){return "<pre>"+escapeHtml( inp.replace(/(?:\r\n|\r|\n)/g, '\n<br>') )+"</pre>"}
            },
            {
                question:"When was this project published / started?",
                result: "",
                output: "",
                sideboxoutput:"",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){return `**published**: ${inp}`}
            },
            {
                question:"What is the name of your company / collective / the artists involved?",
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){return `**by**: ${inp}`}
            },
            {
                question:"Do you have a website about the project or its creators? Provide full urls, seperated by commas!",
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){
                    let urls = inp.split(",");
                    let out = "";
                    urls.forEach((url)=>{
                        url = url.trim();
                        if (!url.startsWith("http")){url="https://"+url}
                        out = out + `<br>[${url.replace("https://","").replace("http://","")}](${url})`;
                    })
                    return `**website(s)**: ${out}`
                }
            },
            {
                question:"Do you have repositories of code or scripts that you can link to?",
                preselect: ["YES","NO"],
                preselectOutputMethod: function(inp,quest){
                        quest.hidden=!(inp == "YES");
                        quest.placeholder="Provide hyperlinks seperated by commas!"
                },
                hidden: true,
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){
                    let urls = inp.split(",");
                    let out = "";
                    urls.forEach((url)=>{
                        url = url.trim();
                        if (!url.startsWith("http")){url="https://"+url}
                        out = out + `<br>[${url.replace("https://","").replace("http://","")}](${url})`;
                    })
                    return `**repositories**: ${out}`
                }
            },
            {
                question:"Do you have a rehearsal blog or another place where you document(ed) your research and rehearsal project?",
                classes: "fellow",
                preselect: ["YES","NO"],
                preselectOutputMethod: function(inp,quest){
                        quest.hidden=!(inp == "YES");
                        quest.placeholder="Provide hyperlink!"
                },
                hidden: true,
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){
                    let urls = inp.split(",");
                    let out = "";
                    urls.forEach((url)=>{
                        url = url.trim();
                        if (!url.startsWith("http")){url="https://"+url}
                        out = out + `<br>[${url.replace("https://","").replace("http://","")}](${url})`;
                    })
                    return `**development/rehearsal blog**:  ${out}`
                }
            },
            {
                question:"What licenses are applied to the project if any? Name them or provide a hyperlink to more information!",
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){
                    let out = linkifyHtml(inp, {defaultProtocol: 'https'});
                    return escapeHtml(`**license(s)**:  ${out}`)
                }
            },
            {
                question:"How can we contact you or the maintainer of the source code or the company?",
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){
                    let out = linkifyHtml(inp, {defaultProtocol: 'https'});
                    return escapeHtml(`**maintainer(s)/contact**:  ${out}`)
                }
            },
            {
                question:"Is your project part of a larger project? If so, please provide a reference to it!",
                preselect: ["YES","NO"],
                preselectOutputMethod: function(inp,quest){
                        quest.hidden=!(inp == "YES");
                        quest.placeholder="Provide hyperlink or other reference!"
                },
                hidden: true,
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){
                    let out = linkifyHtml(inp, {defaultProtocol: 'https'});
                    return escapeHtml(`**part of**:  ${out}`)
                }
            },
            {
                question:"Who cues the show?",
                type:"select",
                selectValues:["humans","machines","humans and machines"],
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return ""},
                sideboxOutputMethod: function(inp){ return escapeHtml(`**cued by**:  ${inp}`)}
            },
            {
                question:"do you want to add some search tags?",
                preselect: ["YES","NO"],
                preselectOutputMethod: function(inp,quest){
                        quest.hidden=!(inp == "YES");
                        quest.placeholder="please add search tags of your choice, seperated by comma!"
                },
                hidden: true,
                result: "",
                output: "",
                sideboxoutput: "",
                outputMethod: function(inp){return `{{tag>[${inp}]}}` // TODO: validate before adding
                },
                sideboxOutputMethod: function(inp){
                    return `**tags**: ${inp}`
                },
                footer: true
            },
            {
                question:"what is your favorite color?",
                result: "",
                output: "",
                outputMethod: function(inp){return "<br><br>Your favorite color is: "+`<span style="color:${inp}">`+inp+"</span>"}
            }
        ]
    }
})


function escapeHtml(unsafe) {
    return unsafe
         .replace(/&/g, "&amp;")
         .replace(/</g, "&lt;")
         .replace(/>/g, "&gt;")
         .replace(/"/g, "&quot;")
         .replace(/'/g, "&#039;");
 }

 function checkFellowProject(){
     let fellowObjects = vm.questionaire.filter((quest)=>{return quest.classes && quest.classes.includes("fellow") })
     console.log(fellowObjects);

     fellowObjects.forEach((fellow)=>{
         console.log(fellow.preselected);
         fellow.preselected = "YES";
         fellow.classes = fellow.classes + " importantfield";
         fellow.preselectOutputMethod(fellow.preselected,fellow)
     })
     
 }

 document.getElementById("copyToClipboard_btn").addEventListener("click",()=>{
    document.getElementById("outputClipboard").value = document.getElementById("output").innerText;
    document.getElementById("outputClipboard").select();
    document.execCommand("copy");
 })

 